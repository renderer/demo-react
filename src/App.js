import React, { Component } from 'react';
import Button from './containers/Button';
import Input from './containers/Input';
import Display from './containers/Display';

const App = () => (
  <React.Fragment>
    <Button />
    <Input />
    <Display />
  </React.Fragment>
);

export default App;
