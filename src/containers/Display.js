import { connect } from 'react-redux';
import Display from '../components/Display';

const mapStateToProps = state => ({
  num: state.number.num,
  text: state.text.text
});

export default connect(mapStateToProps)(Display);
