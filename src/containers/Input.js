import { connect } from 'react-redux';
import Input from '../components/Input';

function mapDispatchToProps(dispatch) {
  return {
    onTextChange: text => dispatch({ type: 'CHANGE_TEXT', text })
  };
}

export default connect(null, mapDispatchToProps)(Input);
