import { connect } from 'react-redux';
import Button from '../components/Button';

function mapDispatchToProps(dispatch) {
  return {
    onButtonClick: () => dispatch({ type: 'INCREASE_NUMBER' })
  };
}

export default connect(null, mapDispatchToProps)(Button);
