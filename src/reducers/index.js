import { combineReducers } from 'redux';
import number from './number';
import text from './text';

export default combineReducers({
  number,
  text
});
