import React from 'react';

export default class Input extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: ''
    };
  }
  render() {
    const { onTextChange } = this.props;
    const { text } = this.state;
    return (
      <input
        value={text}
        onChange={e => {
          onTextChange(e.target.value);
          this.setState({ text: e.target.value });
        }}
      />
    );
  }
}
