import React from 'react';

export default ({ num, text }) => (
  <em>{`Your number is ${num}! Your text is ${text}`}</em>
);
