import React from 'react';

export default ({ onButtonClick }) => (
  <button onClick={onButtonClick}>
    Click me
  </button>
);
